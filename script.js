







const canvas = document.getElementById('canvas')

let element = null
let counter = 1




canvas.addEventListener('click', (event) => {

    element = document.createElement('div');
    let span = document.createElement('span')
    span.style.height = "30px"
    span.style.width = "30px"
    span.innerHTML = `${counter}`
    span.style.fontSize = '30px'
    counter++
    element.appendChild(span)



    element.className = 'rectangle'
    element.classList.add("d-flex", "justify-content-center", "align-items-center")
    element.style.width = Math.round(Math.random(50 - 400) * 400) + 'px'
    element.style.height = Math.round(Math.random(50 - 400) * 400) + 'px'
    element.style.top = event.clientY + 'px'
    element.style.left = event.clientX + 'px'
    element.style.background = `rgb(${Math.round(Math.random(0 - 255) * 255)},${Math.round(Math.random(0 - 255) * 255)},${Math.round(Math.random(0 - 255) * 255)})`

    canvas.appendChild(element)

    PointerEvent.pointerId++


    element.addEventListener('click', () => {
        element.classList.add('d-none')
    })

})










